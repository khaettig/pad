from django.test import TestCase

from wrapper.models import Pad


class PadModelTest(TestCase):

    def test_creating_pads(self):
        first_pad = Pad()
        first_pad.title = 'My first pad'
        first_pad.save()

        second_pad = Pad()
        second_pad.title = 'My second pad'
        second_pad.save()

        saved_pads = Pad.objects.all()
        self.assertEqual(saved_pads.count(), 2)

        self.assertEqual(saved_pads[0].title, 'My first pad')
        self.assertEqual(saved_pads[1].title, 'My second pad')

    def test_creating_password_unprotected_pads(self):
        pad = Pad()
        pad.title = 'A pad without a password'
        pad.save()

        self.assertFalse(Pad.objects.all()[0].password)

    def test_creating_password_protected_pads(self):
        pad = Pad()
        pad.title = 'A pad with a password'
        pad.password = '123'
        pad.save()

        self.assertEqual(Pad.objects.all()[0].password, '123')

    def test_creating_nonpublic_pads(self):
        pad = Pad()
        pad.title = 'A non-public pad'
        pad.save()

        self.assertFalse(Pad.objects.all()[0].is_public)

    def test_creating_public_pads(self):
        pad = Pad()
        pad.title = 'A public pad'
        pad.is_public = True
        pad.save()

        self.assertTrue(Pad.objects.all()[0].is_public)
