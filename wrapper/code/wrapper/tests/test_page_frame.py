from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase


class PageFrameTest(TestCase):

    def create_test_user(self):
        user = User.objects.create_user('test', 'test@test.com', 'password')
        self.client.force_login(user)
        return user

    def test_unauthorized_users_dont_see_admin_link(self):
        response = self.client.get(reverse('login'))

        self.assertNotContains(response, 'ADMIN')

    def test_non_staff_users_dont_see_admin_link(self):
        self.create_test_user()

        response = self.client.get(reverse('index'))

        self.assertNotContains(response, 'ADMIN')

    def test_staff_users_see_admin_link(self):
        user = self.create_test_user()
        user.is_staff = True
        user.save()

        response = self.client.get(reverse('index'))

        self.assertContains(response, 'ADMIN')

    def test_super_users_see_admin_link(self):
        user = self.create_test_user()
        user.is_superuser = True
        user.save()

        response = self.client.get(reverse('index'))

        self.assertContains(response, 'ADMIN')
