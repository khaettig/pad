from unittest.mock import patch
from django.contrib.auth.models import User
from django.test import TestCase
from django.conf import settings
from django.urls import reverse

from wrapper.models import Pad
from wrapper.etherpad_utils import get_etherpad_url


class PadPageTest(TestCase):

    def test_pad_page_requires_authentication(self):
        response = self.client.post(
            reverse('pad', kwargs={'title': 'test'}))
        self.assertRedirects(
            response, '%s?next=%s' % (
                reverse('login'), reverse('pad', kwargs={'title': 'test'})))

    @patch('wrapper.views.etherpad_utils')
    def test_public_pads_dont_require_authentication(self, utils_mock):
        Pad.objects.create(title='testpad', is_public=True)
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': '', 'title': 'testpad', 'group_id': 'g.123'}

        response = self.client.get(reverse('pad', kwargs={'title': 'testpad'}))
        self.assertTemplateUsed(response, 'pad.html')
        self.assertIn('/p/g.123$testpad', response.content.decode())


class AuthorizedPadPageTest(TestCase):

    def setUp(self):
        User.objects.create(username='test')
        self.client.force_login(User.objects.get(username='test'))

    @patch('wrapper.views.etherpad_utils')
    def test_pad_page_returns_rendered_pad_html(self, utils_mock):
        Pad.objects.create(title='testpad1')
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': get_etherpad_url(), 'title': 'testpad1',
            'group_id': 'g.123'}

        response = self.client.get(
            reverse('pad', kwargs={'title': 'testpad1'}))

        self.assertTemplateUsed(response, 'pad.html')
        self.assertIn('%s/p/g.123$testpad1' % get_etherpad_url(),
                      response.content.decode())

    @patch('wrapper.views.etherpad_utils')
    def test_pad_sets_correct_session_cookie(self, utils_mock):
        Pad.objects.create(title='testpad_2')
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': get_etherpad_url(), 'title': 'testpad_2',
            'group_id': 'g.123'}
        utils_mock.create_session_id.return_value = 's.123'

        self.client.get(reverse('pad', kwargs={'title': 'testpad_2'}))

        cookie = self.client.cookies.get('sessionID')
        self.assertEqual('s.123', cookie.value)
        self.assertEqual(f'.{settings.HOST}', cookie['domain'])

    @patch('wrapper.views.etherpad_utils')
    def test_pad_page_updates_last_visited_timestamp(self, utils_mock):
        Pad.objects.create(title='testpad_three')
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': '', 'title': '', 'group_id': ''}

        before = Pad.objects.first().last_visited
        self.client.get(reverse('pad', kwargs={'title': 'testpad_three'}))
        after = Pad.objects.first().last_visited

        self.assertTrue((after - before).microseconds > 0)

    @patch('wrapper.views.etherpad_utils')
    def test_pad_page_evaluates_password_cookie(self, utils_mock):
        Pad.objects.create(title='pwpad', password='pw')
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': '', 'title': '1', 'group_id': 'g.123'}

        response = self.client.get(reverse('pad', kwargs={'title': 'pwpad'}))

        self.assertIn('id_get_pad_password', response.content.decode())
        self.assertNotIn('iframe', response.content.decode())

    @patch('wrapper.views.etherpad_utils')
    def test_pad_page_sets_password_cookie_if_password_right(self, utils_mock):
        Pad.objects.create(title='pw pad', password='pw')
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': '', 'title': '1', 'group_id': 'g.123'}

        self.client.post(
            reverse('pad', kwargs={'title': 'pw pad'}), data={'password': 'pw'})

        etherpad_cookie = self.client.cookies.get('password')
        self.assertEqual('pw', etherpad_cookie.value)
        self.assertEqual(f'.{settings.HOST}', etherpad_cookie['domain'])
        self.assertEqual('/p/g.123$1', etherpad_cookie['path'])
        wrapper_cookie = self.client.cookies.get('wrapper_password')
        self.assertEqual('pw', wrapper_cookie.value)
        self.assertEqual(settings.HOST, wrapper_cookie['domain'])
        self.assertEqual('/pad/pw%20pad', wrapper_cookie['path'])

    @patch('wrapper.views.etherpad_utils')
    def test_pad_page_asks_again_if_password_wrong(self, utils_mock):
        Pad.objects.create(title='pwpad', password='pw')
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': '', 'title': '1', 'group_id': 'g.123'}

        response = self.client.post(
            reverse('pad', kwargs={'title': 'pwpad'}), data={'password': 'p'})

        self.assertIn('falsch', response.content.decode())
        self.assertFalse('password' in self.client.cookies)
        self.assertFalse('wrapper_password' in self.client.cookies)
