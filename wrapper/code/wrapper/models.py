from django.db import models
from django.db.models.fields import DateTimeField, BooleanField

FORBIDDEN_CHARACTERS = 'Der Titel enthält ungültige Zeichen!'


class Pad(models.Model):
    title = models.CharField(max_length=80, unique=True)
    last_visited = DateTimeField(auto_now=True)
    password = models.CharField(max_length=80, default='')
    is_public = BooleanField(default=False)
