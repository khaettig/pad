from django.contrib.auth.models import User
from selenium.common.exceptions import NoSuchElementException

from functional_tests.custom_test_case import CustomTestCase


class PublicPadTest(CustomTestCase):

    def setUp(self):
        self.set_up_browser()
        User.objects.create_user(
            'stagingtestuser', 'stagingtestuser@stura.org', self.test_password)

    def tearDown(self):
        self.tear_down_browser()

    def test_user_can_create_public_pad(self):
        # A user wants to create a new pad. First she logs in.
        self.browser.get(self.live_server_url + '/login/')
        self.enter_credentials('stagingtestuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()

        # She is redirected to the pad list, where she finds the link to create
        # a new pad.
        self.assertEqual(self.live_server_url + '/',
                         self.browser.current_url)
        self.browser.find_element_by_link_text('NEUES PAD').click()
        self.assertEqual(self.live_server_url + '/create_pad/',
                         self.browser.current_url)

        # She enters a title for the pad, checks the public option and
        # clicks enter.
        self.send_input_to_textbox('id_title', 'ft public testpad')
        self.browser.find_element_by_name('is_public').click()
        self.browser.find_element_by_id('id_create_pad').click()

        # She is successful and is redirected to her new pad
        self.assertEqual(self.live_server_url + '/pad/ft%20public%20testpad/',
                         self.browser.current_url)
        self.browser.switch_to.frame('etherpad')
        self.browser.switch_to.frame('ace_outer')
        self.browser.switch_to.default_content()

        # She notices the globe symbol on the page header.
        self.browser.find_element_by_id('id_public')

        # She wants to return to the pad list and sees there also the globe
        # icon.
        self.browser.find_element_by_link_text('Zurück').click()
        link = self.browser.find_element_by_link_text('ft public testpad')
        images = link.parent.find_elements_by_tag_name('img')
        self.assertTrue(any(
            ['globe_black' in str(image.get_attribute('src')).split('/')[-1]
             for image in images]
        ))

        # She logs out and returns to the public pad page where she has access
        # rights.
        self.browser.find_element_by_link_text('ABMELDEN').click()
        self.browser.get(self.live_server_url + '/pad/ft%20public%20testpad')
        self.browser.switch_to.frame('etherpad')
        self.browser.switch_to.default_content()

        # After that she logs back in and deletes the pad.
        self.browser.get(self.live_server_url + '/login/')
        self.enter_credentials('stagingtestuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()
        self.browser.find_element_by_id('id_delete_ft public testpad').click()
        self.assertEqual(
            self.live_server_url + '/pad/ft%20public%20testpad/delete/',
            self.browser.current_url)
        self.browser.find_element_by_id('id_delete_submit').click()
        self.assertEqual(self.live_server_url + '/', self.browser.current_url)
        with self.assertRaises(NoSuchElementException):
            self.browser.find_element_by_link_text('ft public testpad')
