from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.core.exceptions import ValidationError
from django import forms

from wrapper.models import Pad

EMPTY_PAD_TITLE = 'Der Titel darf nicht leer sein!'
PAD_ALREADY_EXISTS = 'Ein Pad mit dem selben Titel existiert bereits!'
PASSWORD_INCORRECT = 'Das alte Passwort stimmt nicht!'
PASSWORDS_DIFFERENT = 'Die neuen Passwörter stimmen nicht überein!'
PASSWORD_TOO_SHORT = 'Das neue Passwort muss mindestens 8 Zeichen lang sein!'
PASSWORD_IS_NUMERIC = 'Das neue Passwort darf nicht nur Zahlen enthalten!'
PASSWORD_IS_ALPHA = 'Das neue Passwort darf nicht nur Buchstaben enthalten!'
USER_ALREADY_EXISTS = 'Ein Account mit dem selben Namen existiert bereits!'


class NewPadForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput, required=False)

    class Meta:
        model = Pad
        fields = ('title', 'password', 'is_public')
        error_messages = {
            'title': {
                'required': EMPTY_PAD_TITLE,
                'unique': PAD_ALREADY_EXISTS}}

    def clean_title(self):
        title = self.cleaned_data['title']

        if title is '':
            raise ValidationError(EMPTY_PAD_TITLE)

        return title


class PadAccessPasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)


class PasswordSetForm(forms.Form):
    new_password1 = forms.CharField(widget=forms.PasswordInput)
    new_password2 = forms.CharField(widget=forms.PasswordInput)

    @staticmethod
    def validate_password(password):
        if len(password) < 8:
            raise ValidationError(PASSWORD_TOO_SHORT)
        if password.isnumeric():
            raise ValidationError(PASSWORD_IS_NUMERIC)
        if password.isalpha():
            raise ValidationError(PASSWORD_IS_ALPHA)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise ValidationError(PASSWORDS_DIFFERENT)
        self.validate_password(password2)
        return password2


class NewUserForm(forms.ModelForm, PasswordSetForm):

    class Meta:
        model = User
        fields = (
            'username', 'first_name', 'last_name', 'email', 'is_staff')
        error_messages = {
            'username': {
                'unique': USER_ALREADY_EXISTS}}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            if field != 'is_staff':
                self.fields[field].required = True

    def clean_username(self):
        return self.cleaned_data['username'].lower()

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["new_password1"])
        if commit:
            user.save()
        return user


class ChangePasswordForm(PasswordSetForm):
    old_password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def clean_old_password(self):
        old_password = self.cleaned_data['old_password']
        if not check_password(old_password, self.user.password):
            raise ValidationError(PASSWORD_INCORRECT)
        return old_password
