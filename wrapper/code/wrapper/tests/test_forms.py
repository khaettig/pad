# pylint: disable=R0201

from django.test import TestCase
from django.http import QueryDict

from wrapper.forms import NewPadForm, NewUserForm, PadAccessPasswordForm
from wrapper.models import Pad


class NewEmptyPadFormTest(TestCase):

    def test_form_has_correct_id(self):
        form = NewPadForm()
        self.assertIn('id="id_title"', form.as_p())


class NewUserFormTest(TestCase):

    def test_username_is_always_lowercase(self):
        form = NewUserForm(data=QueryDict(
            'username=TestUser&' +
            'first_name=test&' +
            'last_name=user&' +
            'new_password1=testtest123&' +
            'new_password2=testtest123&' +
            'email=testuser@stura.org'
        ))

        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['username'], 'testuser')


class CleanTitleTest(TestCase):

    def test_clean_title_with_valid_title(self):
        form = NewPadForm(data=QueryDict('title=test'))

        self.assertTrue(form.is_valid())

    def test_clean_title_with_existing_title(self):
        Pad.objects.create(title='test')

        form = NewPadForm(data=QueryDict('title=test'))

        self.assertFalse(form.is_valid())

    def test_clean_title_with_unusual_character(self):
        form = NewPadForm(data=QueryDict('title=test$._:)'))

        self.assertTrue(form.is_valid())

    def test_clean_title_allows_numbers(self):
        form = NewPadForm(data=QueryDict('title=test2'))

        self.assertTrue(form.is_valid())

    def test_clean_title_allows_underscore(self):
        form = NewPadForm(data=QueryDict('title=test_'))

        self.assertTrue(form.is_valid())


class PasswordTest(TestCase):

    def test_empty_password(self):
        form = NewPadForm(data=QueryDict('title=test'))
        pad = form.save()

        self.assertFalse(pad.password)

    def test_with_password(self):
        form = NewPadForm(data=QueryDict('title=test&password=123'))
        pad = form.save()

        self.assertEquals(pad.password, '123')


class PadAccessPasswordTest(TestCase):

    def test_password(self):
        Pad.objects.create(title='testpad', password='123')
        form = PadAccessPasswordForm(data=QueryDict('password=123'))

        form.is_valid()

        self.assertEquals(form.cleaned_data['password'], '123')


class PublicTest(TestCase):

    def test_create_nonpublic_pad(self):
        form = NewPadForm(data=QueryDict('title=test'))
        pad = form.save()

        self.assertFalse(pad.is_public)

    def test_create_public_pad(self):
        form = NewPadForm(data=QueryDict('title=test&is_public=true'))
        pad = form.save()

        self.assertTrue(pad.is_public)

