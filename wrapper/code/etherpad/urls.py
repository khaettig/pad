"""etherpad URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth import views as auth_views
from django.urls import path  # pylint: disable=W0611
from wrapper import views

urlpatterns = [  # pylint: disable=C0103
    path('', views.index_page, name='index'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', views.logout_page, name='logout'),
    path('help/', views.help_page, name='help'),
    path('privacy_notice/', views.privacy_notice_page, name='privacy_notice'),
    path('about/', views.about_page, name='about'),
    path('pad/<title>/', views.pad_page, name='pad'),
    path('pad/<title>/delete/', views.delete_pad_page, name='delete_pad'),
    path('create_pad/', views.create_pad_page, name='create_pad'),
    path('account/', views.account_page, name='account'),
    path('admin/', views.admin_page, name='admin'),
    path('admin/<account_name>/delete/', views.delete_account_page,
         name='delete_account'),
    path('password_reset/',
         auth_views.PasswordResetView.as_view(),
         name='password_reset'),
    path('password_reset/email_sent/',
         auth_views.PasswordResetDoneView.as_view(),
         name='password_reset_done'),
    path('password_reset/token/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(),
         name='password_reset_confirm'),
    path('password_reset/done',
         auth_views.PasswordResetCompleteView.as_view(),
         name='password_reset_complete')
]
