cd demo
docker-compose build && docker-compose up -d
sleep 10  # give the database some time
echo "Please enter a password for the testuser:"
docker exec -it pad_wrapper python3 manage.py createsuperuser --username testuser --email testuser@not.valid
echo "You can now login at 127.0.0.1:8000 with the credentials testuser and your password."
