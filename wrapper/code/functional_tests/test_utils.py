from pyvirtualdisplay import Display
from etherpad_lite import EtherpadException
from wrapper.etherpad_utils import CustomClient
from wrapper.models import Pad


def get_display(visible=0, size=(800, 600)):
    display = Display(visible=visible, size=size)
    display.start()
    return display


def create_pad_if_not_exists(title):
    Pad.objects.create(title=title)

    client = CustomClient()
    group_id = client.get_group_id('users')
    try:
        client.createGroupPad(groupID=group_id, padName=title)
    except EtherpadException:
        pass


def delete_pad_if_exists(title):
    try:
        Pad.objects.get(title=title).delete()
    except Pad.DoesNotExist:
        pass

    client = CustomClient()
    group_id = client.get_group_id('users')
    try:
        client.deletePad(padID='%s$%s' % (group_id, title))
    except EtherpadException:
        pass
