#! /bin/bash

while ! nc -z $WRAPPER_DB_URL 5432; do
    echo "Waiting for database to come online"
    sleep 1
done

if ! [ -z "$DEBUG" ]; then
  sed -i "s/^DEBUG = True/DEBUG = $DEBUG/g" etherpad/settings.py
fi

if ! [ -z "$USE_IP_AS_DOMAIN" ]; then
  sed -i "s/^COOKIE_PREFIX = '.'/COOKIE_PREFIX = ''/g" etherpad/settings.py
fi

sed -i "s/^SECRET_KEY = 'changeme'/SECRET_KEY = '$WRAPPER_SECRET_KEY'/g" etherpad/settings.py

sed -i "s/^HOST = 'pad.local'/HOST = '$VIRTUAL_HOST'/g" etherpad/settings.py
sed -i "s/^ETHERPAD_HOST = 'etherpad.pad.local'/ETHERPAD_HOST = '$ETHERPAD_HOST'/g" etherpad/settings.py
sed -i "s/^ETHERPAD_INTERNAL_HOST = 'etherpad.pad.local'/ETHERPAD_INTERNAL_HOST = '$ETHERPAD_INTERNAL_HOST'/g" etherpad/settings.py
sed -i "s/^ETHERPAD_INTERNAL_PORT = '80'/ETHERPAD_INTERNAL_PORT = '9001'/g" etherpad/settings.py
sed -i "s/^ETHERPAD_APIKEY = 'changeme'/ETHERPAD_APIKEY = '$ETHERPAD_APIKEY'/g" etherpad/settings.py

sed -i "s/^PROTOCOL = 'http'/PROTOCOL = '$PROTOCOL'/g" etherpad/settings.py
sed -i "s/^EMAIL_HOST_PASSWORD = ''/EMAIL_HOST_PASSWORD = '$EMAIL_HOST_PASSWORD'/g" etherpad/settings.py

sed -i "s/^DB_URL = 'sqlite'/DB_URL = '$WRAPPER_DB_URL'/g" etherpad/settings.py

python3 manage.py migrate

supervisord -n
