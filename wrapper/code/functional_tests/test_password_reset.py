from unittest import skip

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.models import User
from django.core import mail
from django.conf import settings
from selenium import webdriver

from functional_tests.custom_test_case import CustomTestCase
from functional_tests.test_utils import get_display


class PasswordResetTest(CustomTestCase):

    def setUp(self):
        self.set_up_browser()
        User.objects.create_user('testuser', 'test@user.com', 'right_password')

    def tearDown(self):
        self.tear_down_browser()

    def test_can_reset_password(self):
        # TODO: Implement this for the staging server
        if self.staging_server:
            return
        # A user has forgotten his password and wants to reset it.
        # He opens the login page and clicks the link to the reset page.
        self.browser.get(self.live_server_url + '/login/')
        self.browser.find_element_by_id('id_password_reset').click()

        # He reaches the password reset page and can enter his email.
        self.assertEqual(self.live_server_url + '/password_reset/',
                         self.browser.current_url)
        self.send_input_to_textbox('id_email', 'test@user.com')
        self.browser.find_element_by_id('id_send_email').click()

        # The site tells him, that an email has been sent, as long as the email
        # adress he entered belongs to an actual account.
        self.assertEqual(self.live_server_url + '/password_reset/email_sent/',
                         self.browser.current_url)

        # He checks his email and finds one with a link to reset his password.
        self.assertEqual(len(mail.outbox), 1)
        reset_link = [line for line in mail.outbox[0].body.splitlines()
                      if line.startswith(f'{settings.PROTOCOL}://')][0]

        # He follows the link and sees a page where he can reset his password.
        self.browser.get(reset_link)
        self.assertTrue(self.browser.current_url.endswith('/set-password/'))

        # He enters a new password and submits it.
        self.send_input_to_textbox('id_new_password1', 'new_password')
        self.send_input_to_textbox('id_new_password2', 'new_password')
        self.browser.find_element_by_id('id_change_password').click()
        self.assertTrue(User.objects.get(username='testuser')
                            .check_password('new_password'))
