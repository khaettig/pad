from unittest.mock import patch
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve, reverse

from wrapper.views import delete_pad_page
from wrapper.models import Pad


class DeletePadPageTest(TestCase):

    def test_delete_page_resolves_to_delete_pad_page(self):
        self.assertEqual(
            resolve(reverse('delete_pad', kwargs={'title': 'test'})).func,
            delete_pad_page)

    def test_delete_page_redirects_to_login_page_if_not_authenticated(self):
        response = self.client.get(reverse(
            'delete_pad', kwargs={'title': 'test'}))
        self.assertRedirects(response, '/login/?next=/pad/test/delete/')


class AuthorizedPadPageTest(TestCase):

    def setUp(self):
        User.objects.create(username='test')
        self.client.force_login(User.objects.get(username='test'))

    @patch('wrapper.views.etherpad_utils.delete_pad')
    def test_delete_pad_page_redirects_to_index_after_deletion(self, *_):
        response = self.client.post(reverse(
            'delete_pad', kwargs={'title': 'test'}))

        self.assertRedirects(response, reverse('index'))

    @patch('wrapper.views.etherpad_utils.delete_pad')
    def test_delete_pad_page_deletes_pad_from_database(self, *_):
        Pad.objects.create(title='test')

        self.client.post(reverse('delete_pad', kwargs={'title': 'test'}))

        self.assertEqual(0, Pad.objects.count())

    @patch('wrapper.views.etherpad_utils.delete_pad')
    def test_delete_pad_page_deletes_pad_from_etherpad(self, delete_mock):
        pad = Pad.objects.create(title='test')

        self.client.post(reverse('delete_pad', kwargs={'title': 'test'}))

        delete_mock.assert_called_with(str(pad.id))
