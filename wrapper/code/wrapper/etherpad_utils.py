import logging
import time
from etherpad_lite import EtherpadLiteClient, EtherpadException
from django.conf import settings


class CustomClient(EtherpadLiteClient):
    def __init__(self):
        super().__init__(base_params={'apikey': settings.ETHERPAD_APIKEY},
                         base_url=f'{get_internal_etherpad_url()}/api')

    def get_author_id(self, username):
        return self.createAuthorIfNotExistsFor(
            name=username, authorMapper=username)['authorID']

    def get_group_id(self, groupname):
        return self.createGroupIfNotExistsFor(groupMapper=groupname)['groupID']

    def get_session_ids(self, username):
        author_id = self.get_author_id(username)
        sessions = self.listSessionsOfAuthor(authorID=author_id)
        if sessions is None:
            return []
        return sessions.keys()


def create_pad(title, password='', public=False):
    client = CustomClient()
    group_id = client.get_group_id('users')
    try:
        client.createGroupPad(groupID=group_id, padName=title)['padID']
    except EtherpadException as e:
        if str(e) != 'padName does already exist':
            raise e
        else:
            logging.getLogger().warning(
                f'Pad with id { title } already existed.')
    pad_id = f'{ group_id }${ title }'
    client.setPassword(padID=pad_id, password=password)
    client.setPublicStatus(padID=pad_id, publicStatus=str(public).lower())


def delete_pad(title):
    client = CustomClient()
    group_id = client.get_group_id('users')
    try:
        client.deletePad(padID='%s$%s' % (group_id, title))
    except EtherpadException as e:
        if str(e) != 'padID does not exist':
            raise e


def get_etherpad_url():
    return f'{settings.PROTOCOL}://' + \
        f'{settings.ETHERPAD_HOST}:{settings.ETHERPAD_PORT}'


def get_internal_etherpad_url():
    return f'http://' + \
        f'{settings.ETHERPAD_INTERNAL_HOST}:{settings.ETHERPAD_INTERNAL_PORT}'


def get_pad_page_context(title):
    return {'etherpad_url': get_etherpad_url(), 'title': title,
            'group_id': CustomClient().get_group_id('users')}


def create_session_id(username):
    client = CustomClient()
    return client.createSession(
        groupID=client.get_group_id('users'),
        authorID=client.get_author_id(username),
        validUntil=str(int(time.time()) + 3 * 60 * 60))['sessionID']


def logout(username):
    client = CustomClient()
    session_ids = client.get_session_ids(username)
    for session_id in session_ids:
        try:
            client.deleteSession(sessionID=session_id)
        except EtherpadException:
            pass
