import os
from django.contrib.auth.models import User

User.objects.create_superuser(
    'stagingsuperuser',
    'stagingsuperuser@stura.org',
    os.environ.get('STAGING_TESTUSER_PASSWORD'))
admin = User.objects.create_user(
    'stagingadminuser',
    'stagingadminuser@stura.org',
    os.environ.get('STAGING_TESTUSER_PASSWORD'))
admin.is_staff = True
admin.save()
User.objects.create_user(
    'stagingtestuser',
    'stagingtestuser@stura.org',
    os.environ.get('STAGING_TESTUSER_PASSWORD'))
