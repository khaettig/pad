from unittest.mock import patch
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve, reverse

from wrapper.views import logout_page


class LogoutPageTest(TestCase):

    @patch('wrapper.etherpad_utils.logout')
    def test_logout_url_resolves_to_logout_page_view(self, _):
        self.assertEqual(resolve(reverse('logout')).func, logout_page)


class AuthenticatedLogoutPageTest(TestCase):

    def setUp(self):
        User.objects.create(username='test')
        self.client.force_login(User.objects.get(username='test'))

    @patch('wrapper.views.etherpad_utils')
    def test_logout_page_closes_django_session(self, *_):
        self.client.get(reverse('logout'))

        self.assertNotIn('_auth_user_id', self.client.session)

    @patch('wrapper.views.etherpad_utils.logout')
    def test_logout_page_closes_etherpad_session(self, logout_mock):
        self.client.get(reverse('logout'))

        logout_mock.assert_called_with('test')
