from django.contrib.auth.models import User

from functional_tests.custom_test_case import CustomTestCase


class HelpPagesTest(CustomTestCase):

    def setUp(self):
        self.set_up_browser()
        self.test_user = User.objects.create_user(
            'stagingtestuser', 'stagingtestuser@stura.org', self.test_password)

    def tearDown(self):
        self.tear_down_browser()

    def test_user_without_permission_can_see_help_pages(self):
        # A user visits the homepage and wants to read all help pages.
        self.browser.get(self.live_server_url + '/help/')
        self.assertNotIn('Not Found', self.browser.page_source)
        self.browser.get(self.live_server_url + '/privacy_notice/')
        self.assertNotIn('Not Found', self.browser.page_source)
        self.browser.get(self.live_server_url + '/about/')
        self.assertNotIn('Not Found', self.browser.page_source)

    def test_user_with_permission_can_see_help_pages(self):
        # Another user want to see the help pages, but he is logged in.
        self.browser.get(self.live_server_url + '/login/')
        self.enter_credentials('stagingtestuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()

        self.browser.get(self.live_server_url + '/help/')
        self.assertNotIn('Not Found', self.browser.page_source)
        self.browser.get(self.live_server_url + '/privacy_notice/')
        self.assertNotIn('Not Found', self.browser.page_source)
        self.browser.get(self.live_server_url + '/about/')
        self.assertNotIn('Not Found', self.browser.page_source)
