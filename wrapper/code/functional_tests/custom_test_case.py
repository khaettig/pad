from os import environ
from selenium import webdriver
from functional_tests.test_utils import get_display

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.conf import settings


class CustomTestCase(StaticLiveServerTestCase):
    host = settings.HOST

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.staging_server = environ.get('STAGING_SERVER')
        self.test_password = environ.get('TEST_PASSWORD')
        if self.staging_server:
            self.live_server_url = self.staging_server
        if not self.test_password:
            self.test_password = 'test_password'

    def set_up_browser(self):
        if 'SHOW_DISPLAY' not in environ:
            self.display = get_display()
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tear_down_browser(self):
        self.browser.quit()
        if 'SHOW_DISPLAY' not in environ:
            self.display.stop()

    def restart_browser(self):
        """
        Restart the selenium browser. This can be used to clear all cookies
        (Selenium can't delete domain wide cookies).
        """
        self.browser.quit()
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def send_input_to_textbox(self, textbox_id, text):
        textbox = self.browser.find_element_by_id(textbox_id)
        textbox.clear()
        textbox.send_keys(text)

    def enter_credentials(self, username, password):
        self.send_input_to_textbox('id_username', username)
        self.send_input_to_textbox('id_password', password)

