DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

$DIR/cleanup.sh
$DIR/copy_volumes.sh
docker-compose build && docker-compose up -d
echo "Creating staging users"
docker exec pad_staging_wrapper /bin/bash -c "python3 /code/manage.py shell < /code/functional_tests/create_staging_users.py"
