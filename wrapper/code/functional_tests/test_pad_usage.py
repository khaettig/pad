
from django.contrib.auth.models import User
from selenium.common.exceptions import NoSuchElementException, \
    NoSuchFrameException

from functional_tests.custom_test_case import CustomTestCase


class PadDeletionTest(CustomTestCase):

    def setUp(self):
        self.set_up_browser()
        User.objects.create_user(
            'stagingtestuser', 'stagingtestuser@stura.org', self.test_password)

    def tearDown(self):
        self.tear_down_browser()

    def test_user_can_create_edit_and_delete_pad(self):
        # A user wants to create a new pad. First she logs in.
        self.browser.get(self.live_server_url + '/login/')
        self.enter_credentials('stagingtestuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()

        # She is redirected to the pad list, where she finds the link to create
        # a new pad.
        self.assertEqual(self.live_server_url + '/',
                         self.browser.current_url)
        self.browser.find_element_by_link_text('NEUES PAD').click()
        self.assertEqual(self.live_server_url + '/create_pad/',
                         self.browser.current_url)

        # She enters a title for the pad and clicks enter.
        self.send_input_to_textbox('id_title', 'ft testpad')
        self.browser.find_element_by_id('id_create_pad').click()

        # She is successful and is redirected to her new pad.
        self.assertEqual(self.live_server_url + '/pad/ft%20testpad/',
                         self.browser.current_url)

        pad_url = self.browser.find_element_by_id('id_etherpad') \
            .get_attribute('src')

        # She also has the permissions to edit the pad.
        self.browser.switch_to.frame('etherpad')
        self.browser.switch_to.frame('ace_outer')
        self.browser.switch_to.default_content()

        # After she is done editing she wants to return to the pad list.
        self.browser.find_element_by_link_text('Zurück').click()

        # She wants to create another pad but mistakenly uses the same name.
        # The site tells her so.
        self.browser.find_element_by_link_text('NEUES PAD').click()
        self.send_input_to_textbox('id_title', 'ft testpad')
        self.browser.find_element_by_id('id_create_pad').click()
        self.assertIn('existiert bereits', self.browser.page_source)

        # On the pad list she logs out.
        self.browser.find_element_by_link_text('ABMELDEN').click()

        # But she remembers to edit one last thing on the pad so she returns to
        # the link but since she isn't authenticated anymore the pad doesn't
        # let her see it's content.
        self.browser.get(self.live_server_url + '/pad/ft%20testpad/')
        self.assertEqual(
            self.live_server_url + '/login/?next=/pad/ft%20testpad/',
            self.browser.current_url)
        self.browser.get(pad_url)
        with self.assertRaises(NoSuchFrameException):
            self.browser.switch_to.frame('ace_outer')

        # So she logs back in.
        self.browser.get(self.live_server_url + '/login/')
        self.enter_credentials('stagingtestuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()

        # Now she wants to delete the pad.
        self.browser.find_element_by_id('id_delete_ft testpad').click()
        self.assertEqual(self.live_server_url + '/pad/ft%20testpad/delete/',
            self.browser.current_url)

        # She is asked if she if she really wants to delete the pad. On second
        # thought she doesn't want to delete it, presses no, and is directed
        # back to the index page.
        self.browser.find_element_by_id('id_back_submit').click()
        self.assertEqual(self.live_server_url + '/', self.browser.current_url)

        # On third thought she wants to delete the pad after all and clicks
        # again on delete.
        self.browser.find_element_by_id('id_delete_ft testpad').click()
        self.assertEqual(self.live_server_url + '/pad/ft%20testpad/delete/',
            self.browser.current_url)

        # Now she clicks on delete and is redirected to the index page where
        # she sees that the pad disappeared.
        self.browser.find_element_by_id('id_delete_submit').click()
        self.assertEqual(self.live_server_url + '/', self.browser.current_url)
        with self.assertRaises(NoSuchElementException):
            self.browser.find_element_by_link_text('ft testpad')
