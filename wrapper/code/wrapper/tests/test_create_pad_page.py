from unittest.mock import patch
from django.contrib.auth.models import User
from django.test import TestCase
from django.conf import settings
from django.utils.html import escape
from django.urls import reverse

from wrapper.forms import NewPadForm, EMPTY_PAD_TITLE, PAD_ALREADY_EXISTS
from wrapper.models import Pad, FORBIDDEN_CHARACTERS
from wrapper.etherpad_utils import get_etherpad_url


class CreatePadPageTest(TestCase):

    def test_create_pad_page_requires_authentication(self):
        response = self.client.post(
            reverse('create_pad'), data={'title': 'test'})
        self.assertRedirects(
            response, '%s?next=%s' % (reverse('login'), reverse('create_pad')))


class AuthorizedCreatePadPageTest(TestCase):

    def setUp(self):
        User.objects.create(username='test')
        self.client.force_login(User.objects.get(username='test'))

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_returns_correct_html(self, *_):
        response = self.client.get(reverse('create_pad'))

        self.assertTemplateUsed(response, 'create_pad.html')

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_uses_new_pad_form(self, *_):
        response = self.client.get(reverse('create_pad'))

        self.assertIsInstance(response.context['form'], NewPadForm)

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_doesnt_take_empty_pad_names(self, *_):
        response = self.client.post(
            reverse('create_pad'), data={'title': ''})

        self.assertContains(response, escape(EMPTY_PAD_TITLE))

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_doesnt_create_already_existing_pad(self, *_):
        Pad.objects.create(title='test')
        response = self.client.post(
            reverse('create_pad'), data={'title': 'test'})

        self.assertContains(response, escape(PAD_ALREADY_EXISTS))

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_can_create_new_pad(self, utils_mock):
        data = {'title': 'test'}
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': get_etherpad_url(), 'title': 'testpad1',
            'group_id': 'g.123'}

        response = self.client.post(reverse('create_pad'), data=data)

        utils_mock.create_pad.assert_called_with(
            title='1', password='', public=False)
        self.assertRedirects(response, reverse('pad', kwargs=data))

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_passes_password(self, utils_mock):
        data = {'title': 'test', 'password': '123'}
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': get_etherpad_url(), 'title': 'testpad1',
            'group_id': 'g.123'}

        self.client.post(reverse('create_pad'), data=data)

        utils_mock.create_pad.assert_called_with(
            title='1', password='123', public=False)

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_passes_public_status(self, utils_mock):
        data = {'title': 'test', 'is_public': 'true'}
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': get_etherpad_url(), 'title': 'testpad1',
            'group_id': 'g.123'}

        self.client.post(reverse('create_pad'), data=data)

        utils_mock.create_pad.assert_called_with(
            title='1', password='', public=True)

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_uses_post_data_only_from_post_requests(self, *_):
        response = self.client.get(reverse('create_pad'))

        self.assertNotContains(response, EMPTY_PAD_TITLE)

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_accepts_unusual_characters_in_pad_title(
            self, utils_mock):
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': get_etherpad_url(), 'title': '1',
            'group_id': 'g.123'}
        response = self.client.post(
            reverse('create_pad'), data={'title': '.:$&_"'})

        utils_mock.create_pad.assert_called_with(
            title='1', password='', public=False)
        self.assertRedirects(
            response, reverse('pad', kwargs={'title': '.:$&_"'}))

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_creates_new_pad_in_database(self, *_):
        self.client.post(reverse('create_pad'), data={'title': 'test'})

        self.assertEqual(1, Pad.objects.count())

    @patch('wrapper.views.etherpad_utils')
    def test_create_pad_page_sets_password_cookie(self, utils_mock):
        utils_mock.get_pad_page_context.return_value = {
            'etherpad_url': get_etherpad_url(), 'title': '1',
            'group_id': 'g.123'}

        self.client.post(
            reverse('create_pad'), data={'title': 'test', 'password': 'pw'})

        cookie = self.client.cookies.get('password')
        self.assertEqual('pw', cookie.value)
        self.assertEqual(f'.{settings.HOST}', cookie['domain'])
        self.assertEqual('/p/g.123$1', cookie['path'])
