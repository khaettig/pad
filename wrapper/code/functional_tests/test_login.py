from django.contrib.auth.models import User

from functional_tests.custom_test_case import CustomTestCase


class LoginTest(CustomTestCase):

    def setUp(self):
        self.set_up_browser()
        User.objects.create_user(
            'stagingtestuser', 'stagingtestuser@stura.org', self.test_password)

    def tearDown(self):
        self.tear_down_browser()

    def test_can_login_from_index(self):
        # A user wants to use the pad system and calls the index page.
        self.browser.get(self.live_server_url)

        # She isn't logged in and therefore is redirected to the login page.
        self.assertIn('Login - StuRa Uni Freiburg', self.browser.title)
        self.assertEqual(self.live_server_url + '/login/?next=/',
                         self.browser.current_url)

        # She enters her credentials but mistypes her password. After she
        # presses the login button the site tells her, that her username or
        # password was wrong.
        self.enter_credentials('stagingtestuser', 'wrong_password')
        self.browser.find_element_by_id('id_login_submit').click()
        self.assertIn('stimmen nicht', self.browser.page_source)
        self.assertEqual(self.live_server_url + '/login/',
                         self.browser.current_url)

        # She enters her credentials correctly, presses the login button, and
        # is redirected to the index page.
        self.enter_credentials('stagingtestuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()
        self.assertEqual(self.live_server_url + '/', self.browser.current_url)
