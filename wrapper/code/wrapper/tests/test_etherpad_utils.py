# pylint: disable=R0201

from unittest.mock import MagicMock, patch, call
from django.test import TestCase
from django.conf import settings

from wrapper import etherpad_utils


class EtherpadUtilsTest(TestCase):

    @patch('wrapper.etherpad_utils.EtherpadLiteClient.__init__')
    def test_custom_client_uses_correct_api_key(self, init_mock):
        etherpad_utils.CustomClient()
        init_mock.assert_called_with(
            base_params={'apikey': settings.ETHERPAD_APIKEY},
            base_url=f'http://{settings.ETHERPAD_INTERNAL_HOST}:80/api')

    @patch('time.time')
    @patch('wrapper.etherpad_utils.CustomClient')
    def test_utils_creates_correct_session_id(self, class_mock, time_mock):
        time_mock.return_value = 657284820.0
        client_mock = MagicMock()
        client_mock.get_author_id.return_value = 'a.123'
        client_mock.get_group_id.return_value = 'g.123'
        class_mock.return_value = client_mock

        etherpad_utils.create_session_id('testuser')

        client_mock.get_author_id.assert_called_with('testuser')
        client_mock.get_group_id.assert_called_with('users')
        client_mock.createSession.assert_called_with(
            groupID='g.123', authorID='a.123', validUntil='657295620')

    @patch('wrapper.etherpad_utils.CustomClient')
    def test_utils_get_pad_page_context(self, class_mock):
        client_mock = MagicMock()
        client_mock.get_group_id.return_value = 'g.123'
        class_mock.return_value = client_mock

        context = etherpad_utils.get_pad_page_context('testpad')

        self.assertEqual(context['group_id'], 'g.123')
        self.assertEqual(context['title'], 'testpad')

    @patch('wrapper.etherpad_utils.CustomClient')
    def test_logout_deletes_sessions(self, class_mock):
        client_mock = MagicMock()
        client_mock.get_session_ids.return_value = ['s.123', 's.456']
        class_mock.return_value = client_mock

        etherpad_utils.logout('testuser')

        client_mock.get_session_ids.assert_called_with('testuser')
        client_mock.deleteSession.assert_has_calls(
            [call(sessionID='s.123'), call(sessionID='s.456')])

    @patch('wrapper.etherpad_utils.CustomClient')
    def test_logout_with_no_etherpad_sessions(self, class_mock):
        client_mock = MagicMock()
        client_mock.get_session_ids.return_value = ['s.123', 's.456']
        class_mock.return_value = client_mock

        etherpad_utils.logout('testuser')

        client_mock.get_session_ids.assert_called_with('testuser')
        client_mock.deleteSession.assert_has_calls([])

    @patch('wrapper.etherpad_utils.CustomClient')
    def test_create_pad_makes_correct_api_call(self, class_mock):
        client_mock = MagicMock()
        client_mock.createGroupPad.return_value = {'padID': 'g.123$testpad'}
        client_mock.get_group_id.return_value = 'g.123'
        class_mock.return_value = client_mock

        etherpad_utils.create_pad('testpad')

        client_mock.get_group_id.assert_called_with('users')
        client_mock.createGroupPad.assert_called_with(
            groupID='g.123', padName='testpad')

    @patch('wrapper.etherpad_utils.CustomClient')
    def test_create_pad_with_password_makes_correct_api_call(self, class_mock):
        client_mock = MagicMock()
        client_mock.createGroupPad.return_value = {'padID': 'g.123$testpad'}
        client_mock.get_group_id.return_value = 'g.123'
        class_mock.return_value = client_mock

        etherpad_utils.create_pad('testpad', password='password')

        client_mock.get_group_id.assert_called_with('users')
        client_mock.createGroupPad.assert_called_with(
            groupID='g.123', padName='testpad')
        client_mock.setPassword.assert_called_with(
            padID='g.123$testpad', password='password')

    @patch('wrapper.etherpad_utils.CustomClient')
    def test_create_public_pad_makes_correct_api_call(self, class_mock):
        client_mock = MagicMock()
        client_mock.createGroupPad.return_value = {'padID': 'g.123$testpad'}
        client_mock.get_group_id.return_value = 'g.123'
        class_mock.return_value = client_mock

        etherpad_utils.create_pad('testpad', public=True)

        client_mock.get_group_id.assert_called_with('users')
        client_mock.createGroupPad.assert_called_with(
            groupID='g.123', padName='testpad')
        client_mock.setPublicStatus.assert_called_with(
            padID='g.123$testpad', publicStatus='true')

    @patch('wrapper.etherpad_utils.CustomClient')
    def test_delete_pad_deletes_pad(self, class_mock):
        client_mock = MagicMock()
        client_mock.get_group_id.return_value = 'g.123'
        class_mock.return_value = client_mock

        etherpad_utils.delete_pad('testpad')

        client_mock.deletePad.assert_called_with(padID='g.123$testpad')
