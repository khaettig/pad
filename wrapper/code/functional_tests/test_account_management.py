from django.contrib.auth.models import User

from functional_tests.custom_test_case import CustomTestCase


class AccountManagementTest(CustomTestCase):
    def setUp(self):
        self.set_up_browser()
        User.objects.create_user(
            'stagingtestuser', 'stagingtestuser@stura.org', self.test_password)

    def tearDown(self):
        self.tear_down_browser()

    def test_user_can_change_password(self):
        # A user wants to change his password. Therefore he logs in.
        self.browser.get(self.live_server_url + '/login/')
        self.enter_credentials('stagingtestuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()

        # He is redirected to the index page, sees the link to the account
        # management page, clicks it and is redirected to the account page.
        self.assertEqual(self.live_server_url + '/', self.browser.current_url)
        self.browser.find_element_by_link_text('MEIN ACCOUNT').click()
        self.assertEqual(self.live_server_url + '/account/',
                         self.browser.current_url)

        # On the account management page he can enter a new password. He
        # mistypes the old password on the first try. The site tells him so.
        self.send_input_to_textbox('id_old_password', 'wrong_password')
        self.send_input_to_textbox('id_new_password1', 'new_password')
        self.send_input_to_textbox('id_new_password2', 'new_password')
        self.browser.find_element_by_id('id_change_password_submit').click()
        self.assertIn('alte Passwort stimmt nicht', self.browser.page_source)

        # On the second try he mistypes his new password. Again the site tells
        # him so.
        self.send_input_to_textbox('id_old_password', self.test_password)
        self.send_input_to_textbox('id_new_password1', 'new_password')
        self.send_input_to_textbox('id_new_password2', 'new_passwort')
        self.browser.find_element_by_id('id_change_password_submit').click()
        self.assertIn('Passwörter stimmen nicht', self.browser.page_source)

        # On the third try he gets it right. The site tells him, that his
        # password was successfully changed.
        self.send_input_to_textbox('id_old_password', self.test_password)
        self.send_input_to_textbox('id_new_password1', self.test_password)
        self.send_input_to_textbox('id_new_password2', self.test_password)
        self.browser.find_element_by_id('id_change_password_submit').click()
        self.assertIn('erfolgreich geändert', self.browser.page_source)
