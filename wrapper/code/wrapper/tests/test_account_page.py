from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.urls import resolve, reverse
from django.test import TestCase

from wrapper.views import account_page


class AccountPageTest(TestCase):

    def test_account_url_resolves_to_account_page_view(self):
        self.assertEqual(resolve(reverse('account')).func, account_page)

    def test_account_page_redirects_to_login_page_if_not_authenticated(self):
        response = self.client.get(reverse('account'))
        self.assertRedirects(response, '/login/?next=/account/')


class AuthorizedAccountPageTest(TestCase):

    def setUp(self):
        User.objects.create_user('test', 'test@test.com', 'old_password')
        self.client.force_login(User.objects.get(username='test'))

    @staticmethod
    def has_password(username, password):
        return check_password(
            password, User.objects.get(username=username).password)

    def test_change_password_changes_password_with_valid_input(self):
        self.client.post(reverse('account'), data={
            'old_password': 'old_password',
            'new_password1': 'new_password',
            'new_password2': 'new_password'})

        self.assertTrue(self.has_password('test', 'new_password'))

    def test_change_password_requires_old_password(self):
        self.client.post(reverse('account'), data={
            'old_password': 'wrong_password',
            'new_password1': 'new_password',
            'new_password2': 'new_password'})

        self.assertTrue(self.has_password('test', 'old_password'))

    def test_change_password_needs_matching_new_passwords(self):
        self.client.post(reverse('account'), data={
            'old_password': 'old_password',
            'new_password1': 'new_password',
            'new_password2': 'new_passwort'})

        self.assertTrue(self.has_password('test', 'old_password'))

    def test_new_passwords_need_to_satisfy_safety_criteria(self):
        self.client.post(reverse('account'), data={
            'old_password': 'old_password',
            'new_password1': 'new',
            'new_password2': 'new'})

        self.assertTrue(self.has_password('test', 'old_password'))

    def test_user_stays_logged_in_after_password_change(self):
        self.client.post(reverse('account'), data={
            'old_password': 'old_password',
            'new_password1': 'new_password',
            'new_password2': 'new_password'})

        self.assertTrue(auth.get_user(self.client).is_authenticated)
