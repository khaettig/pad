#!/bin/bash

docker volume create sturapadstaging_wrapper_data
docker volume create sturapadstaging_etherpad_data

docker run --rm -it -v sturapad_wrapper_data:/from -v sturapadstaging_wrapper_data:/to alpine /bin/sh -c "cp -a /from/* /to"
docker run --rm -it -v sturapad_etherpad_data:/from -v sturapadstaging_etherpad_data:/to alpine /bin/sh -c "cp -a /from/* /to"
