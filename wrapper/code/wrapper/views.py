import urllib

from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth import logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.utils.http import urlencode

from wrapper import etherpad_utils
from wrapper.forms import NewPadForm, PadAccessPasswordForm, \
    ChangePasswordForm, NewUserForm
from wrapper.models import Pad

PASSWORD_CHANGED = 'Passwort erfolgreich geändert!'
MISSING_PERMISSION = \
    'Du besitzt nicht die nötigen Rechte um diese Seite zu sehen!'
CANT_DELETE_SUPERUSER = \
    'Dieser Account ist Superuser und kann nur über das Django-Interface ' + \
    'gelöscht werden!'
ACCOUNT_DOESNT_EXIST = 'Dieser Account existiert nicht!'


def touch_pad(title):
    Pad.objects.get(title=title).save()


@login_required
def index_page(request):
    return render(
        request, 'index.html',
        {'pads': reversed(Pad.objects.all().order_by('last_visited'))})


def help_page(request):
    return render(request, 'help.html')


def privacy_notice_page(request):
    return render(request, 'privacy_notice.html')


def about_page(request):
    return render(request, 'about.html')


def pad_page(request, title):
    try:
        pad = Pad.objects.get(title=title)
    except Pad.DoesNotExist:
        # TODO: 404
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    if not pad.is_public and not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    context = etherpad_utils.get_pad_page_context(str(pad.id))
    if request.POST:
        if request.POST['password'] == pad.password:
            etherpad_path = f'/p/{ context["group_id"]}${ context["title"]}'
            wrapper_path = f'/pad/{ title}'
            response = redirect(reverse('pad', kwargs={'title': title}))
            response.set_cookie(
                key='wrapper_password',
                value=pad.password,
                domain=settings.HOST,
                path=urllib.parse.quote(wrapper_path))
            response.set_cookie(
                key='password',
                value=pad.password,
                domain=f'.{settings.HOST}',
                path=etherpad_path)
            return response
        else:
            context['wrong_password'] = True
    touch_pad(title)
    context['pad'] = pad
    if pad.password and not is_password_cookie_correct(request, pad.password):
        context['get_password_form'] = PadAccessPasswordForm()
    response = render(request, 'pad.html', context)

    if request.user.is_authenticated:
        session_id = etherpad_utils.create_session_id(request.user.username)
        response.set_cookie(
            key='sessionID',
            value=session_id,
            domain=f'{settings.COOKIE_PREFIX}{settings.HOST}')
    return response


def is_password_cookie_correct(request, password):
    return request.COOKIES.get('wrapper_password') == password


@login_required
def create_pad_page(request):
    if request.method == 'POST':
        form = NewPadForm(data=request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            pad = form.save()
            etherpad_utils.create_pad(
                title=str(pad.id),
                password=form.cleaned_data['password'],
                public=form.cleaned_data['is_public'])
            context = etherpad_utils.get_pad_page_context(str(pad.id))
            etherpad_path = f'/p/{ context["group_id"]}${ context["title"]}'
            wrapper_path = f'/pad/{ form.cleaned_data["title"]}'
            response = redirect(reverse('pad', kwargs={'title': title}))
            response.set_cookie(
                key='wrapper_password',
                value=form.cleaned_data['password'],
                domain=settings.HOST,
                path=urllib.parse.quote(wrapper_path))
            response.set_cookie(
                key='password',
                value=form.cleaned_data['password'],
                domain=f'.{settings.HOST}',
                path=etherpad_path)
            return response
    else:
        form = NewPadForm()
    return render(request, 'create_pad.html', {'form': form})


@login_required
def delete_pad_page(request, title):
    if request.method == 'POST':
        if Pad.objects.filter(title=title).exists():
            pad = Pad.objects.get(title=title)
            etherpad_utils.delete_pad(str(pad.id))
            pad.delete()
        return redirect(reverse('index'))
    return render(request, 'delete_pad.html', {'title': title})


@login_required
def account_page(request):
    user = User.objects.get(id=request.user.id)
    context = {'form': ChangePasswordForm(user)}
    if request.method == 'POST':
        form = ChangePasswordForm(user, data=request.POST)
        if form.is_valid():
            user.password = make_password(form.cleaned_data['new_password1'])
            user.save()
            update_session_auth_hash(request, user)
            context['message'] = PASSWORD_CHANGED
        else:
            return render(request, 'account.html', {'form': form})
    return render(request, 'account.html', context)


@login_required
def admin_page(request):
    user = User.objects.get(id=request.user.id)
    form = NewUserForm()
    if not user.is_staff and not user.is_superuser:
        return render(request, 'error.html', {'message': MISSING_PERMISSION})
    if request.method == 'POST':
        form = NewUserForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('admin'))
    return render(
        request, 'admin.html', {'form': form, 'users': User.objects.all()})


@login_required
def delete_account_page(request, account_name):
    user = User.objects.get(id=request.user.id)
    if not user.is_staff and not user.is_superuser:
        return render(request, 'error.html', {'message': MISSING_PERMISSION})
    if not User.objects.filter(username=account_name):
        return render(request, 'error.html', {'message': ACCOUNT_DOESNT_EXIST})
    if request.method == 'POST':
        user_to_delete = User.objects.get(username=account_name)
        if user_to_delete.is_superuser:
            return render(
                request, 'error.html', {'message': CANT_DELETE_SUPERUSER})
        user_to_delete.delete()
        return redirect(reverse('admin'))
    return render(request, 'delete_account.html', {'username': account_name})


def logout_page(request):
    etherpad_utils.logout(request.user.username)
    logout(request)
    return redirect(settings.LOGOUT_REDIRECT_URL)
