#! /bin/bash

sed -i "s/\"ip\": "0.0.0.0"/\"ip\": "$VIRTUAL_HOST"/g"settings.json
sed -i "s/\"password\": \"changeme\",/\"password\": \"$MYSQL_ROOT_PASSWORD\",/g" settings.json
sed -i "s/\"host\"    : \"pad_etherpad_db\",/\"host\"    : \"$MYSQL_HOST\",/g" settings.json
echo $ETHERPAD_APIKEY > APIKEY.txt
export NODE_ENV=production

./bin/run.sh --root
