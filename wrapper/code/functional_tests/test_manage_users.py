from django.contrib.auth.models import User
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

from functional_tests.custom_test_case import CustomTestCase
from functional_tests.test_utils import get_display


class UserManagementTest(CustomTestCase):

    def setUp(self):
        self.set_up_browser()
        self.test_user = User.objects.create_user(
            'stagingtestuser', 'stagingtestuser@stura.org', self.test_password)
        self.test_superuser = User.objects.create_superuser(
            'stagingsuperuser',
            'stagingsuperuser@stura.org',
            self.test_password)
        User.objects.create_user(
            'stagingadminuser',
            'stagingadminuser@stura.org',
            self.test_password
        )

    def tearDown(self):
        self.tear_down_browser()

    def test_user_without_permission_cant_see_admin(self):
        # A user visits the homepage and wonders if she can edit other users.
        self.browser.get(self.live_server_url + '/login/')
        self.enter_credentials('stagingtestuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()

        # She is redirected to the index page but can't see the link to the
        # admin page.
        self.assertEqual(self.live_server_url + '/', self.browser.current_url)
        with self.assertRaises(NoSuchElementException):
            self.browser.find_element_by_link_text('ADMIN')

        # She tries to enter the link manually but the site tells her that she
        # doesn't have the needed permissions.
        self.browser.get(self.live_server_url + '/admin/')
        self.assertIn('nicht die nötigen Rechte', self.browser.page_source)

    def test_user_with_permission_can_create_and_delete_accounts(self):
        # A superuser user visits the homepage and needs to manage some
        # accounts.
        self.browser.get(self.live_server_url + '/login/')
        self.enter_credentials('stagingsuperuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()

        # He is redirected to the index page, sees the link to the admin page,
        # and clicks it.
        self.assertEqual(self.live_server_url + '/', self.browser.current_url)
        self.browser.find_element_by_link_text('ADMIN').click()
        self.assertEqual(
            self.live_server_url + '/admin/', self.browser.current_url)

        # He sees a list of all existing users.
        self.assertIn('stagingtestuser', self.browser.page_source)
        self.assertIn('stagingadminuser', self.browser.page_source)
        self.assertIn('stagingsuperuser', self.browser.page_source)

        # Next he wants to create a new account. He enters the name, the email
        # address and the twice the password on the admin page and presses
        # enter. But the site tells him, that he mistyped the password.
        self.send_input_to_textbox('id_username', 'newstaginguser')
        self.send_input_to_textbox('id_first_name', 'new')
        self.send_input_to_textbox('id_last_name', 'new')
        self.send_input_to_textbox('id_email', 'newstaginguser@stura.org')
        self.send_input_to_textbox('id_new_password1', 'the_password')
        self.send_input_to_textbox('id_new_password2', 'the_passwort')
        self.browser.find_element_by_id('id_create_user_submit').click()

        # He corrects his error and clicks enter.
        self.send_input_to_textbox('id_new_password1', 'the_password')
        self.send_input_to_textbox('id_new_password2', 'the_password')
        self.browser.find_element_by_id('id_create_user_submit').click()

        # He now sees the new created user in the list.
        self.assertIn('newstaginguser', self.browser.page_source)

        # On second thought he doesn't want to have this new account,
        # so he deletes it.
        self.browser.find_element_by_id('id_delete_newstaginguser').click()
        self.assertEqual(
            self.live_server_url + '/admin/newstaginguser/delete/',
            self.browser.current_url)

        # The site asks him, if he is sure and he answers yes. He is
        # redirected to the admin page where he sees that 'newstaginguser' is
        # gone.
        self.browser.find_element_by_id('id_delete_submit').click()
        self.assertEqual(
            self.live_server_url + '/admin/', self.browser.current_url)
        self.assertNotIn('newstaginguser', self.browser.page_source)
