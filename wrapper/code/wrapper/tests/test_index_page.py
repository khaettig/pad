from django.contrib.auth.models import User
from django.urls import resolve, reverse
from django.test import TestCase

from wrapper.views import index_page
from wrapper.models import Pad


class IndexPageTest(TestCase):

    def test_root_url_resolves_to_index_page_view(self):
        self.assertEqual(resolve(reverse('index')).func, index_page)

    def test_index_page_redirects_to_login_page_if_not_authenticated(self):
        response = self.client.get(reverse('index'))
        self.assertRedirects(response, '/login/?next=/')


class AuthorizedIndexPageTest(TestCase):

    def setUp(self):
        User.objects.create(username='test')
        self.client.force_login(User.objects.get(username='test'))

    @staticmethod
    def is_before(content, before, after):
        return content.find(before) < content.find(after)

    def test_index_page_returns_correct_html(self):
        response = self.client.get(reverse('index'))

        self.assertTemplateUsed(response, 'index.html')

    def test_index_page_shows_existing_pads(self):
        Pad.objects.create(title='testpad_1')
        Pad.objects.create(title='testpad_2')

        response = self.client.get(reverse('index'))

        self.assertContains(response, 'testpad_1')
        self.assertContains(response, 'testpad_2')

    def test_index_page_shows_last_visited_first(self):
        pad1 = Pad.objects.create(title='pad_1')
        pad2 = Pad.objects.create(title='pad_2')
        pad3 = Pad.objects.create(title='pad_3')
        # This updates the last_visited field since auto_now is set.
        pad2.save()
        pad3.save()
        pad1.save()

        content = self.client.get(reverse('index')).content.decode('utf-8')

        self.assertTrue(self.is_before(content, 'pad_1', 'pad_3'))
        self.assertTrue(self.is_before(content, 'pad_3', 'pad_2'))
