from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase

from wrapper.views import MISSING_PERMISSION, CANT_DELETE_SUPERUSER, \
    ACCOUNT_DOESNT_EXIST


class DeleteAccountPageTest(TestCase):

    def test_delete_account_page_redirects_to_login_page_if_not_authed(self):
        response = self.client.get(
            reverse('delete_account', kwargs={'account_name': 'test'}))

        self.assertRedirects(response, '/login/?next=/admin/test/delete/')

    def test_delete_account_page_shows_error_for_users_without_perm(self):
        user = User.objects.create(username='test')
        self.client.force_login(user)

        response = self.client.get(
            reverse('delete_account', kwargs={'account_name': 'test'}))

        self.assertContains(response, MISSING_PERMISSION)


class AuthorizedAdminPageTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='test')
        self.user.is_staff = True
        self.user.save()
        self.client.force_login(self.user)

    def test_delete_account_page_redirects_to_admin_after_deletion(self):
        User.objects.create(username='deleteme')

        response = self.client.post(reverse(
            'delete_account', kwargs={'account_name': 'deleteme'}))

        self.assertRedirects(response, reverse('admin'))

    def test_cant_delete_superuser(self):
        superuser = User.objects.create(username='dontdeleteme')
        superuser.is_superuser = True
        superuser.save()

        response = self.client.post(reverse(
            'delete_account', kwargs={'account_name': 'dontdeleteme'}))

        self.assertContains(response, CANT_DELETE_SUPERUSER)

    def test_cant_delete_nonexistent_user(self):
        response = self.client.post(reverse(
            'delete_account', kwargs={'account_name': 'doesntexist'}))

        self.assertContains(response, ACCOUNT_DOESNT_EXIST)
