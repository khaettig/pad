docker rm -f \
  pad_staging_wrapper \
  pad_staging_wrapper_db \
  pad_staging_etherpad \
  pad_staging_etherpad_db

docker volume rm \
  sturapadstaging_wrapper_data \
  sturapadstaging_etherpad_data
