from django.urls import resolve, reverse
from django.test import TestCase

from wrapper.views import help_page, privacy_notice_page, about_page


class HelpPageTest(TestCase):

    def test_help_url_resolves_to_index_page_view(self):
        self.assertEqual(resolve(reverse('help')).func, help_page)

    def test_help_page_returns_correct_html(self):
        response = self.client.get(reverse('help'))

        self.assertTemplateUsed(response, 'help.html')


class PrivacyNoticePageTest(TestCase):

    def test_privacy_notice_url_resolves_to_index_page_view(self):
        self.assertEqual(
            resolve(reverse('privacy_notice')).func, privacy_notice_page)

    def test_privacy_notice_page_returns_correct_html(self):
        response = self.client.get(reverse('privacy_notice'))

        self.assertTemplateUsed(response, 'privacy_notice.html')


class AboutPageTest(TestCase):

    def test_about_url_resolves_to_index_page_view(self):
        self.assertEqual(resolve(reverse('about')).func, about_page)

    def test_about_page_returns_correct_html(self):
        response = self.client.get(reverse('about'))

        self.assertTemplateUsed(response, 'about.html')
