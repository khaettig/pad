This is a wrapper around [Etherpad Lite](https://etherpad.org/) that I wrote for the Students' Union of the University of Freiburg. It provides a basic interface to manage users and a variety of different pads.

### Used Stack
* nginx
* uwsgi
* django
* postgres
* docker

### Usage
To spin up a demo, clone the repository and execute the `demo.sh` script. Note, that you need docker and docker-compose installed. After running the script, go to `127.0.0.1:8000` (note that the software is in German, however the functionallity should be clear).

### Structure
This software uses four docker containers plus a reverse proxy (e.g. [ngingx-proxy from jwilder](https://github.com/jwilder/nginx-proxy) + [letsencrypt-companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion)). The first two are a simple etherpad lite installation plus database. The third and fourth are the wrapper plus its database.
Both etherpad and wrapper containers have to be reachable from the web. However, the wrapper container is the only one visible for the users. Internally the wrapper communicates with the HTTP API from etherpad to create pads, users and sessions, and then displays the pad via an iframe.

### Why Django?
At the time of the implementation, we were a little bit in a rush to spin up our own etherpad instance, since the old provider struggled with uptime. I've looked around for alternatives. Etherpad Lite had some old plugins for user management and access rights, but most were outdated or didn't work the way we needed it.
I've decided to implement a wrapper with Django using the HTTP API from etherpad instead of directly programming a plugin for etherpad, since I was already used to Django (and it is fun to work with) and the implementation of a plugin for etherpad seemed rather involved. With Django I could use the framework for most of the critical areas. In the end I think Django was the correct decision and I've saved a lot of time using it.

