from django.contrib.auth.models import User
from selenium.common.exceptions import NoSuchElementException

from functional_tests.custom_test_case import CustomTestCase


class PasswordProtectedPadTest(CustomTestCase):
    def setUp(self):
        self.set_up_browser()
        User.objects.create_user(
            'stagingtestuser', 'stagingtestuser@stura.org', self.test_password)

    def tearDown(self):
        self.tear_down_browser()

    def test_user_can_create_password_protected_pad(self):
        # A user wants to create a new pad. First she logs in.
        self.browser.get(self.live_server_url + '/login/')
        self.enter_credentials('stagingtestuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()

        # She is redirected to the pad list, where she finds the link to create
        # a new pad.
        self.assertEqual(self.live_server_url + '/',
                         self.browser.current_url)
        self.browser.find_element_by_link_text('NEUES PAD').click()
        self.assertEqual(self.live_server_url + '/create_pad/',
                         self.browser.current_url)

        # She enters a title for the pad, enters a password for the pad and
        # clicks enter.
        self.send_input_to_textbox('id_title', 'ft password testpad')
        self.send_input_to_textbox('id_password', 'thisisapassword')
        self.browser.find_element_by_id('id_create_pad').click()

        # She is successful and is redirected to her new pad. She doesn't need
        # to retype the password.
        self.assertEqual(self.live_server_url + '/pad/ft%20password%20testpad/',
                         self.browser.current_url)
        self.browser.switch_to.frame('etherpad')
        self.browser.switch_to.frame('ace_outer')
        self.browser.switch_to.default_content()

        # She notices the lock symbol on the page header.
        self.browser.find_element_by_id('id_password_protected')

        # She wants to return to the pad list and sees there also the lock
        # icon.
        self.browser.find_element_by_link_text('Zurück').click()
        link = self.browser.find_element_by_link_text('ft password testpad')
        images = link.parent.find_elements_by_tag_name('img')
        self.assertTrue(any(
            ['lock_black' in str(image.get_attribute('src')).split('/')[-1]
             for image in images]
        ))

        # She restarts her browser (and all cookies are gone).
        self.restart_browser()

        # She logs back in and tries to access the new pad.
        self.browser.get(self.live_server_url + '/login/')
        self.enter_credentials('stagingtestuser', self.test_password)
        self.browser.find_element_by_id('id_login_submit').click()
        self.browser.find_element_by_link_text('ft password testpad').click()

        # This time the pad asks her for the password (since she didn't just
        # create the pad, and the cookie isn't set).
        self.browser.find_element_by_id('id_password')

        # She enters the password, but makes a small mistake. The page tells
        # her so.
        self.send_input_to_textbox('id_password', 'thisisnotright')
        self.browser.find_element_by_id('id_submit_password').click()
        self.assertIn('falsch', self.browser.page_source)

        # This time she gets it right and is granted access to the pad.
        self.send_input_to_textbox('id_password', 'thisisapassword')
        self.browser.find_element_by_id('id_submit_password').click()
        self.browser.switch_to.frame('etherpad')
        self.browser.switch_to.frame('ace_outer')
        self.browser.switch_to.default_content()

        # She goes back to the index page and deletes the pad.
        self.browser.find_element_by_link_text('Zurück').click()
        self.browser.find_element_by_id('id_delete_ft password testpad').click()
        self.assertEqual(
            self.live_server_url + '/pad/ft%20password%20testpad/delete/',
            self.browser.current_url)
        self.browser.find_element_by_id('id_delete_submit').click()
        self.assertEqual(self.live_server_url + '/', self.browser.current_url)
        with self.assertRaises(NoSuchElementException):
            self.browser.find_element_by_link_text('ft password testpad')
