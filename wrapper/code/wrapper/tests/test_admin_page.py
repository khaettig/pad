from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.urls import resolve, reverse
from django.test import TestCase

from wrapper.views import admin_page, MISSING_PERMISSION


class AdminPageTest(TestCase):

    def test_admin_url_resolves_to_admin_page_view(self):
        self.assertEqual(resolve(reverse('admin')).func, admin_page)

    def test_admin_page_redirects_to_login_page_if_not_authenticated(self):
        response = self.client.get(reverse('admin'))

        self.assertRedirects(response, '/login/?next=/admin/')

    def test_admin_page_shows_error_for_users_without_permission(self):
        user = User.objects.create(username='test')
        self.client.force_login(user)

        response = self.client.get(reverse('admin'))

        self.assertContains(response, MISSING_PERMISSION)


class AuthorizedAdminPageTest(TestCase):

    def setUp(self):
        self.user = User.objects.create(username='test')
        self.user.is_staff = True
        self.user.save()
        self.client.force_login(self.user)

    def test_admin_page_doesnt_show_error_for_users_with_permission(self):
        response = self.client.get(reverse('admin'))

        self.assertNotContains(response, MISSING_PERMISSION)

    def test_admin_page_lists_existing_users(self):
        User.objects.create(username='username')

        response = self.client.get(reverse('admin'))

        self.assertContains(response, 'username')

    def test_redirects_to_admin_after_creation(self):
        response = self.client.post(reverse('admin'), data={
            'username': 'test_user',
            'first_name': 'test',
            'last_name': 'user',
            'email': 'test@user.com',
            'new_password1': 'new_password',
            'new_password2': 'new_password',
            'is_staff': False})

        self.assertRedirects(response, reverse('admin'))

    def test_can_create_normal_account(self):
        self.client.post(reverse('admin'), data={
            'username': 'test_user',
            'first_name': 'test',
            'last_name': 'user',
            'email': 'test@user.com',
            'new_password1': 'new_password',
            'new_password2': 'new_password',
            'is_staff': False})

        self.assertEqual(2, User.objects.count())
        self.assertFalse(User.objects.get(username='test_user').is_staff)

    def test_can_create_staff_account(self):
        self.client.post(reverse('admin'), data={
            'username': 'test_user',
            'first_name': 'test',
            'last_name': 'user',
            'email': 'test@user.com',
            'new_password1': 'new_password',
            'new_password2': 'new_password',
            'is_staff': True})

        self.assertEqual(2, User.objects.count())
        self.assertTrue(User.objects.get(username='test_user').is_staff)

    def test_account_creation_uses_password(self):
        self.client.post(reverse('admin'), data={
            'username': 'test_user',
            'first_name': 'test',
            'last_name': 'user',
            'email': 'test@user.com',
            'new_password1': 'new_password',
            'new_password2': 'new_password',
            'is_staff': False})

        self.assertTrue(check_password(
            'new_password',
            User.objects.get(username='test_user').password))

    def test_account_creation_requires_all_fields(self):
        self.client.post(reverse('admin'), data={
            'username': 'test_user',
            'new_password1': 'new_password',
            'new_password2': 'new_password',
            'is_staff': False})

        self.assertEqual(1, User.objects.count())
